#! python3  # noqa: E265

"""
    Main plugin module.
"""

# standard
from functools import partial
from pathlib import Path
from time import strftime

# PyQGIS
from qgis.core import QgsApplication, QgsSettings, QgsVectorLayer, QgsDistanceArea
from qgis.core import QgsCoordinateTransform, QgsCoordinateReferenceSystem, QgsProject
from qgis.core import QgsNetworkAccessManager, QgsGeometry, QgsPointXY, QgsFeature
from qgis.core import QgsProcessingUtils, QgsProcessingContext
from qgis.gui import QgisInterface, QgsElevationProfileCanvas
from qgis.PyQt.QtCore import QCoreApplication, QLocale, QTranslator, QUrl, QUrlQuery
from qgis.PyQt.QtGui import QDesktopServices, QIcon
from qgis.PyQt.QtWidgets import QAction, QDockWidget, QToolBar
from qgis.PyQt.QtNetwork import QNetworkRequest

# processing
import processing

# project
from ign_profil_altimetrique.__about__ import (
    DIR_PLUGIN_ROOT,
    __icon_path__,
    __title__,
    __uri_homepage__,
)
from ign_profil_altimetrique.gui.dlg_settings import PlgOptionsFactory

from ign_profil_altimetrique.toolbelt import PlgLogger
import ign_profil_altimetrique.toolbelt.preferences as plg_prefs_hdlr

from ign_profil_altimetrique.gui.gui_utils import GuiUtils
from ign_profil_altimetrique.utils import run_alg, suffix_time, distance

import json

# ############################################################################
# ########## Classes ###############
# ##################################


class IgnProfilAltimetriquePlugin:
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.log = PlgLogger().log
        

        # translation
        # initialize the locale
        self.locale: str = QgsSettings().value("locale/userLocale", QLocale().name())[
            0:2
        ]
        locale_path: Path = (
            DIR_PLUGIN_ROOT / "resources" / "i18n" / f"{__title__.lower()}_{self.locale}.qm"
        )
        self.log(message=f"Translation: {self.locale}, {locale_path}", log_level=4)
        if locale_path.exists():
            self.translator = QTranslator()
            self.translator.load(str(locale_path.resolve()))
            QCoreApplication.installTranslator(self.translator)

        self.dock_widget = None
        self.profile_canvas = None
        self.curve = None
        self.elevation_layer = None
        self.elevation_path = None
        self.time_suffix = None
        self.sampling = None

        # Main
        self.dock_opened = False

        # Action to create elevation profile deck
        self.mActionElevation = self.iface.mainWindow().findChild(QAction, "mActionElevationProfile")

        self.manager = QgsNetworkAccessManager.instance()
        self.url = None

        # context
        self.context = QgsProcessingContext()
        self.d = QgsDistanceArea()
        self.d.setEllipsoid('WGS84')

    def initGui(self):
        """Set up plugin UI elements."""

        # settings page within the QGIS preferences menu
        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

        # -- Actions
        self.action_main = QAction(
             GuiUtils.get_icon('icon.svg'), 
             "IGN Profil altimetrique",
             self.iface.mainWindow())
        #self.action.setWhatsThis("IGN ")
        self.action_main.triggered.connect(self.run)

        self.action_help = QAction(
            QgsApplication.getThemeIcon("mActionHelpContents.svg"),
            self.tr("Help"),
            self.iface.mainWindow(),
        )
        self.action_help.triggered.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_homepage__))
        )

        self.action_settings = QAction(
            QgsApplication.getThemeIcon("console/iconSettingsConsole.svg"),
            self.tr("Settings"),
            self.iface.mainWindow(),
        )
        self.action_settings.triggered.connect(
            lambda: self.iface.showOptionsDialog(
                currentPage="mOptionsPage{}".format(__title__)
            )
        )

        # -- Menu
        self.iface.addPluginToMenu(__title__, self.action_main)
        self.iface.addPluginToMenu(__title__, self.action_settings)
        self.iface.addPluginToMenu(__title__, self.action_help)

        # -- Help menu

        # documentation
        self.iface.pluginHelpMenu().addSeparator()
        self.action_help_plugin_menu_documentation = QAction(
            QIcon(str(__icon_path__)),
            f"{__title__} - Documentation",
            self.iface.mainWindow(),
        )
        self.action_help_plugin_menu_documentation.triggered.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_homepage__))
        )

        self.iface.pluginHelpMenu().addAction(
            self.action_help_plugin_menu_documentation
        )

    

    def tr(self, message: str) -> str:
        """Get the translation for a string using Qt translation API.

        :param message: string to be translated.
        :type message: str

        :returns: Translated version of message.
        :rtype: str
        """
        return QCoreApplication.translate(self.__class__.__name__, message)

    def unload(self):
        """Cleans up when plugin is disabled/uninstalled."""
        # -- Clean up menu
        self.iface.removePluginMenu(__title__, self.action_main)
        self.iface.removePluginMenu(__title__, self.action_help)
        self.iface.removePluginMenu(__title__, self.action_settings)

        # -- Clean up preferences panel in QGIS settings
        self.iface.unregisterOptionsWidgetFactory(self.options_factory)

        

        # remove from QGIS help/extensions menu
        if self.action_help_plugin_menu_documentation:
            self.iface.pluginHelpMenu().removeAction(
                self.action_help_plugin_menu_documentation
            )

        # remove actions
        del self.action_main
        del self.action_settings
        del self.action_help

        # remove dock_widget
        if self.dock_widget:
            self.dock_widget.toggleViewAction().trigger()
            del self.dock_widget
    
    def reset(self):
        self.dock_opened = False
        self.dock_widget = None

    def run(self):
        """Main process.

        :raises Exception: if there is no item in the feed
        """
        try:
            self.log(
                message=self.tr("Everything ran OK."),
                log_level=3,
                push=False,
            )
        except Exception as err:
            self.log(
                message=self.tr("Houston, we've got a problem: {}".format(err)),
                log_level=2,
                push=True,
            )
        if not self.dock_opened:
            dock_widgets = self.iface.mainWindow().findChildren(QDockWidget)
            self.mActionElevation.trigger()
            new_dock_widgets = self.iface.mainWindow().findChildren(QDockWidget)
            for d in new_dock_widgets:
                if d not in dock_widgets:
                    self.dock_widget = d
            self.dock_widget.setWindowTitle("IGN Profil Altimetrique")
            # prevent dock_widget to be closed to avoid kill instance
            #self.dock_widget.setFeatures(QDockWidget.DockWidgetMovable | QDockWidget.DockWidgetFloatable)
            self.dock_opened = True
            self.dock_widget.destroyed.connect(self.reset)
            self.profile_canvas = self.dock_widget.findChild(QgsElevationProfileCanvas)
            toolbar = self.dock_widget.findChild(QToolBar)
            self.action_alti = QAction(
                GuiUtils.get_icon('icon.svg'),
                self.tr("Calcul altimétrique par l'API de l'IGN"),
                toolbar)
            self.action_alti.triggered.connect(self.calcul_altimetrique)
            toolbar.addSeparator()
            toolbar.addAction(self.action_alti)
            
        else:
            try:
                self.dock_widget.raise_()
            except:
                pass

    def calcul_altimetrique(self):
        try:
            curve = self.profile_canvas.profileCurve()
            if not curve or curve == self.curve:
                return
            else:
                self.curve = curve.clone()
        except:
            pass
        self.ign_api_rest(self.curve)

    def ign_api_rest(self, curve):
        in_crs = self.profile_canvas.crs()
        if in_crs == QgsCoordinateReferenceSystem.fromEpsgId(3857):
            self.log(
                message="using the 3857 projection could lead to erroneous distance calculations",
                log_level=1,
                push=True,
                duration=5)
        out_crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        transform = QgsCoordinateTransform(in_crs, out_crs, QgsProject.instance())
        curve.transform(transform)
        self.build_url(curve)
    
    def build_url(self, curve):
        url = plg_prefs_hdlr.PlgOptionsManager.get_plg_settings().url
        url = QUrl(url)
        lon = "|".join([str(pt.x()) for pt in curve])
        lat = "|".join([str(pt.y()) for pt in curve])
        query = QUrlQuery()
        query.addQueryItem("lon", lon)
        query.addQueryItem("lat", lat)
        query.addQueryItem("delimiter", "|")
        query.addQueryItem("resource", "ign_rge_alti_wld")
        query.addQueryItem("measures","true")
        query.addQueryItem("zonly","false")

        # sampling
        self.calculate_sampling()
        self.log(f"sampling : {self.sampling}", log_level=4)
        if self.sampling:           
            query.addQueryItem("sampling", str(self.sampling))
            
        url.setQuery(query)
        self.url = url
        self.r = self.manager.get(QNetworkRequest(url))
        self.r.finished.connect(self.on_reply_received)

    def on_reply_received(self):
        rep = self.r.readAll()
        self.data = json.loads(rep.data())
        self.log(message=self.data, log_level=4)
        #self.profile_canvas.clear()
        layer = self.get_elevation_layer()
        if layer and layer.isValid():
            # Z field is at third position (X,Y,Z)
            zmin, zmax = layer.minimumAndMaximumValue(2)
            self.generate_path()
            length = self.calculate_length()
            self.update_profile_canvas(0, length, zmin, zmax)

    
    def update_profile_canvas(self, min_dist = 0, max_dist = 10, min_elev = 0, max_elev = 10):
        self.profile_canvas.refresh()
        # use setVisiblePlotRange() instead of zoomFull
        self.log(message=f"min_dist : {min_dist}, max_dist : {max_dist}, min_elev : {min_elev}, max_elev : {max_elev}")
        self.profile_canvas.setVisiblePlotRange(min_dist, max_dist, min_elev, max_elev)
        self.profile_canvas.refresh()

    def reset_elevation_layer(self):
        self.elevation_layer = None

    def generate_path(self):
        """function generates path"""
        algs ={'INPUT': self.elevation_layer,'CLOSE_PATH':False,'ORDER_EXPRESSION':'','NATURAL_SORT':False,'GROUP_EXPRESSION':'','OUTPUT':'TEMPORARY_OUTPUT'}
        self.elevation_path = processing.run("native:pointstopath", algs)['OUTPUT']
        trace_layername = plg_prefs_hdlr.PlgOptionsManager.get_plg_settings().trace_layername
        self.elevation_path.setName(f"{trace_layername}_{self.time_suffix}")
        QgsProject.instance().addMapLayer(self.elevation_path)
    
    def calculate_length(self):
        if self.elevation_path.isValid():
            features = self.elevation_path.getFeatures()
            path = next(features, None)
            if path:
                geom = path.geometry()
                if geom:
                    length = self.d.measureLength(geom)
                    return(length)

    def get_elevation_layer(self):
        output_file = QgsProcessingUtils.generateTempFilename('elevation.xyz')
        self.log(message=output_file, log_level=4)
        with open(output_file, 'w') as file:
            file.write("X Y Z Source Distance\n")
            prev_pt = None
            dist = 0
            for point in self.data['elevations']:
                # on prend la première mesure
                mes = point['measures'][0]
                x = point['lon']
                y = point['lat']
                if prev_pt is not None:
                    dist = dist + distance(prev_pt['lon'], prev_pt['lat'], x, y)
                file.write(f"""{x} {y} {mes['z']} "{mes['source_name']}" {round(dist,2)}\n""")
                prev_pt = point
        elevation_layername = plg_prefs_hdlr.PlgOptionsManager.get_plg_settings().elevation_layername
        self.time_suffix = suffix_time()
        layer_name = f"{elevation_layername}_{self.time_suffix}"
        uri = f"file:///{output_file}?type=csv&delimiter=%20&maxFields=10000&detectTypes=yes&xField=X&yField=Y&zField=Z&crs=EPSG:4326&spatialIndex=no&subsetIndex=no&watchFile=no"
        self.log(message=uri, log_level=4)
        layer = QgsVectorLayer(uri, layer_name, "delimitedtext")
        if layer.isValid():
            QgsProject.instance().addMapLayer(layer)
            layer.willBeDeleted.connect(self.reset_elevation_layer)
            layer.loadNamedStyle(GuiUtils.get_style('elevation.qml'))
            self.elevation_layer = layer
            return self.elevation_layer
        else:
            return None
        
    def get_parameters_to_calculate_sampling(self):
        """check precision meters and sampling settings, open the settings if both are set to 0"""
        elevation_precision = plg_prefs_hdlr.PlgOptionsManager.get_plg_settings().elevation_precision
        elevation_sampling = plg_prefs_hdlr.PlgOptionsManager.get_plg_settings().elevation_sampling
        if elevation_precision == 0 and elevation_sampling == 0:
            self.log(
                message="Precision step and sampling cannot be both set to 0. You should check your settings",
                log_level=3,
                push=True
            )
        return elevation_precision, elevation_sampling
    
    def calculate_sampling(self):
        """
        calculate sampling based on parameters
        """
        elevation_precision, elevation_sampling = self.get_parameters_to_calculate_sampling()
        sampling = min(self.curve.numPoints(), 5000)
        if not elevation_sampling and not elevation_precision:
            self.sampling = sampling
        if not elevation_precision and elevation_sampling:
            self.sampling = elevation_sampling
        if elevation_precision:
            new_sampling = self.d.measureLength(QgsGeometry(self.curve)) // elevation_precision + 1
            if new_sampling > 5000:
                self.log(
                        message="Sampling cannot be superior at 5000. Using the default max(number_of_vertex, 5000)",
                        log_level=2,
                        push=True
                    )
            else:
                sampling = new_sampling
            if elevation_sampling:
                sampling = max(new_sampling, elevation_sampling)
            self.sampling = sampling




