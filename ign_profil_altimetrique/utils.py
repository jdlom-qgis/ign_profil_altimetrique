#! python3  # noqa: E265

"""
    Utils module.
"""
from time import strftime

from qgis.core import QgsProcessingContext, QgsProcessingFeedback, QgsVectorLayer
from qgis.core import QgsDistanceArea, QgsPointXY
import processing

def run_alg(alg, params, name, context = QgsProcessingContext(), feedback = QgsProcessingFeedback(), is_child_algorithm = True, output = 'OUTPUT') -> QgsVectorLayer:
    layer = processing.run(
        alg,
        params,
        feedback=feedback,
        context=context,
        is_child_algorithm= is_child_algorithm
    )[output]
    if context.willLoadLayerOnCompletion(layer):
        layer_details = context.layerToLoadOnCompletionDetails(layer)
        layer_details.name = name
    return layer

def suffix_time(suffix_tmpl="%Y%m%d%H%M%S") -> str:
    return strftime(suffix_tmpl)

def distance(x1,y1,x2,y2):
    d = QgsDistanceArea()
    d.setEllipsoid('WGS84')
    pt1 = QgsPointXY(x1, y1)
    pt2 = QgsPointXY(x2, y2)
    return d.measureLine(pt1, pt2)
                    