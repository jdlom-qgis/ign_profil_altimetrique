# IGN Profil Altimetrique - QGIS Plugin

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

[![pylint](https://gitlab.com/jdlom-qgis/ign_profil_altimetrique/lint/pylint.svg)](https://gitlab.com/jdlom-qgis/ign_profil_altimetrique/lint/)

## Test version
[Last version of plugin](https://gitlab.com/jdlom-qgis/ign_profil_altimetrique/-/jobs/artifacts/main/download?job=build:plugin)

## Generated options

### Plugin

| Cookiecutter option | Picked value |
| :-- | :--: |
| Plugin name | IGN Profil Altimetrique |
| Plugin name slugified | ign_profil_altimetrique |
| Plugin name class (used in code) | IgnProfilAltimetrique |
| Plugin category | Vector |
| Plugin description short | This tool plot profiles lines by directly querying French IGN API. Working only on the french territory. |
| Plugin description long | Extends QGIS with revolutionary features that every single GIS end-users was expected (or not)! |
| Plugin tags | profile,alti,ign |
| Plugin icon | default_icon.png |
| Plugin with processing provider | False |
| Author name | Jean-Daniel LOMENEDE |
| Author organization | Jean-Daniel LOMENEDE |
| Author email | jdaniel.lomenede@mailo.com |
| Minimum QGIS version | 3.28 |
| Maximum QGIS version | 3.99 |
| Git repository URL | https://gitlab.com/jdlom-qgis/ign_profil_altimetrique/ |
| Git default branch | main |
| License | GPLv2+ |
| Python linter | PyLint |
| CI/CD platform | GitLab |
| IDE | VSCode |

### Tooling

This project is configured with the following tools:

- [Black](https://black.readthedocs.io/en/stable/) to format the code without any existential question
- [iSort](https://pycqa.github.io/isort/) to sort the Python imports

Code rules are enforced with [pre-commit](https://pre-commit.com/) hooks.  
Static code analisis is based on: PyLint

See also: [contribution guidelines](CONTRIBUTING.md).

## CI/CD

Plugin is linted, tested, packaged and published with GitLab.

If you mean to deploy it to the [official QGIS plugins repository](https://plugins.qgis.org/), remember to set your OSGeo credentials (`OSGEO_USER_NAME` and `OSGEO_USER_PASSWORD`) as environment variables in your CI/CD tool. 


### Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages.

- homepage: <https://gitlab.com/jdlom-qgis/ign_profil_altimetrique/>
- repository: <https://gitlab.com/jdlom-qgis/ign_profil_altimetrique/>
- tracker: <https://gitlab.com/jdlom-qgis/ign_profil_altimetrique//-/issues>

----

## Next steps

### Set up development environment

> Typical commands on Linux (Ubuntu).

1. If you don't pick the `git init` option, initialize your local repository:

    ```sh
    git init
    ```

1. Follow the [embedded documentation to set up your development environment](./docs/development/environment.md)
1. Add all files to git index to prepare initial commit:

    ```sh
    git add -A
    ```

1. Run the git hooks to ensure that everything runs OK and to start developing on quality standards:

    ```sh
    pre-commit run -a
    ```

### Try to build documentation locally

1. Have a look to the [plugin's metadata.txt file](ign_profil_altimetrique/metadata.txt): review it, complete it or fix it if needed (URLs, etc.).
1. Follow the [embedded documentation to build plugin documentation locally](./docs/development/environment.md)

----

## License

Distributed under the terms of the [`GPLv2+` license](LICENSE).
