# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->
## 0.5.0 - 2024-10-22
- Add distance mesure

## 0.4.0 - 2024-09-19
- Add measure source

## 0.3.0 - 2024-05-18
- Add sampling parameters

## 0.2.0 - 2024-05-12
- Stable version with parameters

## 0.1.0 - 2023-10-28

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
